'use strict';

angular
  .module('myApp')

  .controller('View1Ctrl', [
    '$scope',
    '$rootScope',
    function($scope, $rootScope) {
      $rootScope.meta = {
        title: 'title 1',
        description: 'description 1'
      };
      $scope.location = window.location.href;
    }
  ]);
