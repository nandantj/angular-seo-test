'use strict';

angular
  .module('myApp')

  .controller('View2Ctrl', [
    '$scope',
    '$rootScope',
    function($scope, $rootScope) {
      $rootScope.meta = {
        title: 'title 2',
        description: 'description 2'
      };
      $scope.location = window.location.href;
    }
  ]);
