'use strict';

angular
  .module('myApp')

  .controller('View3Ctrl', [
    '$scope',
    '$rootScope',
    function($scope, $rootScope) {
      $rootScope.meta = {
        title: 'title 3',
        description: 'description 3'
      };
      $scope.location = window.location.href;
    }
  ]);
